package org.boosey.parking.owner;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import org.boosey.parking.events.OwnerCreatedEvent;
import org.boosey.parking.events.OwnerDeletedEvent;
import org.boosey.parking.events.OwnerIsResidentUpdatedEvent;
import org.boosey.parking.events.OwnerEmailUpdatedEvent;
import org.boosey.parking.events.OwnerNameUpdatedEvent;
import org.boosey.parking.events.OwnerPhoneUpdatedEvent;
import org.boosey.parking.utilities.Utilities;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Data
@Slf4j
@ApplicationScoped
class OwnerService {
    @Inject public Utilities u;

    public class OwnerDoesNotExistException extends RuntimeException {
        static final long serialVersionUID = 1L;
        public OwnerDoesNotExistException() {
            super("Owner doesn't exist");
        }
    }

    public List<Owner> listOwners() {
        return Owner.listAll(); 
    }

    public long deleteAllOwners() {
        long count = Owner.count();
        Owner.<Owner>streamAll().forEach(o -> {
            o.delete();
            OwnerDeletedEvent.with(o.id).emit();
        });
        return count;
    }

    public Owner createOwner(@NonNull Owner o) throws Exception, AssertionError {
        try {
            assert(o.spaceId != null && o.spaceId.length() > 0);
            if(!(Owner.find("spaceId", o.spaceId).firstResultOptional().isPresent())) {
                o.persist(); 
                OwnerCreatedEvent.with(o.id, o.spaceId).emit();
                return o;
            } else {
                log.info("Owner for unit exists - creation failed: {}", o.spaceId);
                return null;
            }
        } catch (AssertionError e) {
            log.error("Unit not specified", e);
            return null;
        } catch (Exception e) {
            log.error("Unknown error creating new owner {}", o);
            return null;
        }
    }

    public Owner getOwnerById(@NonNull String id) throws OwnerDoesNotExistException {
        Optional<Owner> o = Owner.findByIdOptional(new ObjectId(id));
        o.orElseThrow(OwnerDoesNotExistException::new);        
        return o.get();
    } 

    public Boolean changeOwnerProperties(@NonNull String id, @NonNull Owner ownerUpdates) throws OwnerDoesNotExistException, Exception {
        try {
            Owner o = getOwnerById(id);
            ownerUpdates.spaceId = null;
            u.updateFromNonNullFields(o, ownerUpdates).update();

            OwnerNameUpdatedEvent.with(new ObjectId(id), ownerUpdates.name).emitIf(ownerUpdates.name != null);
            OwnerPhoneUpdatedEvent.with(new ObjectId(id), ownerUpdates.phone).emitIf(ownerUpdates.phone != null);
            OwnerEmailUpdatedEvent.with(new ObjectId(id), ownerUpdates.email).emitIf(ownerUpdates.email != null);
            OwnerIsResidentUpdatedEvent.with(new ObjectId(id), ownerUpdates.ownerIsResident.toString()).emitIf(ownerUpdates.ownerIsResident != null);

        } catch (OwnerDoesNotExistException e) {
            throw e;

        } catch (Exception e) {
            throw e;
        }
        
        return true;
    }
}