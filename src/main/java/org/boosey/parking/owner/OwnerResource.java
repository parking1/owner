package org.boosey.parking.owner;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.boosey.parking.owner.OwnerService.OwnerDoesNotExistException;
import lombok.Data;
import lombok.NonNull;

@Data 
@Path("/owner")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class OwnerResource {

    @Inject
    OwnerService service;

    @PUT
    @Path("/{id}")
    public Response updateOwner(@PathParam("id") String id, @NonNull Owner o) {
        try {            
            service.changeOwnerProperties(id, o);
            return Response.ok(service.getOwnerById(id)).build();

        } catch (OwnerDoesNotExistException e) {
            return Response.status(Status.NOT_FOUND).build();
        } catch (Exception e) {
            return Response.status(500).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getOwnerById(@PathParam("id") String id) {
        try {
            return Response.ok(service.getOwnerById(id)).build();

        } catch(OwnerDoesNotExistException e) {
            return Response.status(Status.NOT_FOUND).build();

        } catch(IllegalArgumentException e) {
            return Response.status(Status.BAD_REQUEST).build();

        } catch (RuntimeException e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GET
    public Response listAll(){
        try {
            return Response.ok(service.listOwners()).build(); 
                       
        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    public Response create(@NonNull Owner owner) {
        try {      
            return Response.status(Status.CREATED).entity(service.createOwner(owner)).build();

        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    public Response deleteAll() {
        try {
            return Response.ok(service.deleteAllOwners()).build();   

        } catch (Exception e) {
            return Response.status(Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}