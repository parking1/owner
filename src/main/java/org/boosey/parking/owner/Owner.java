package org.boosey.parking.owner;

import javax.enterprise.context.ApplicationScoped;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import lombok.ToString;

@ToString
@MongoEntity(collection = "owner")
@ApplicationScoped
public class Owner extends PanacheMongoEntity {
    public String name;
    public String email;
    public String phone;
    public String spaceId;
    public Boolean ownerIsResident;
}